#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QSplitter>
#include <configurator.h>
#include <QTreeWidgetItem>
#include "qtreewidgetitemcustom.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT


public slots:
    void onOpenProject();
    void onSaveProject();
public:
    MainWindow(QWidget *parent = nullptr);
    void add_makefile(QString path);
    void add_pcfile(QString path);
    ~MainWindow();

private slots:
    void on_treeWidget_itemClicked(QTreeWidgetItem *item, int column);

    void on_bt_addMakefile_clicked();

    void on_bt_addtarget_clicked();

    void on_bt_delmk_clicked();

    void on_tb_projname_editingFinished();

    void on_tb_mmakepath_editingFinished();

    void on_tb_projvers_editingFinished();

    void on_tb_email_editingFinished();
    void onNewProject();

    void on_bt_maddsubdir_clicked();

    void on_ls_headers_clicked(const QModelIndex &index);

    void on_bt_addheader_clicked();

    void on_bt_delheader_clicked();


    void on_bt_delsubdir_clicked();

    void on_bt_addlib_clicked();

    void on_ls_libraries_clicked(const QModelIndex &index);

    void on_bt_dellib_clicked();

    void on_tb_headername_editingFinished();

    void on_bt_addmod_clicked();

    void on_ls_modules_clicked(const QModelIndex &index);

    void on_bt_delmod_clicked();

    void on_tb_src_editingFinished();

    void on_bt_addPCFile_clicked();

    void on_tb_pcfile_textChanged();


    void on_tb_CPPFLAGS_editingFinished();

    void on_tb_CFLAGS_editingFinished();

    void on_tb_LDFLAGS_editingFinished();

    void on_tb_LDADD_editingFinished();

    void on_tb_LIBADD_editingFinished();

private:
    Ui::MainWindow *ui;
    QString projdir;
    QTreeWidgetItem *topLevelItem;
    std::shared_ptr<autotools::Configurator> autocfg;

    autotools::Makefile_ref selected_makefile;
    autotools::Target_ref selected_target;
    autotools::PCFile_ref selected_pcfile;

    QTreeWidgetItemCustom * selected_node;

    void configureac_selected();
    void makefile_selected( autotools::Makefile_ref mk );
    void target_selected( autotools::Target_ref mk );
    void pcfile_selected(autotools::PCFile_ref pc);
    void ReloadProject();
    void enableui();
    void udpateMKsubdirlist();
    void updateHeaderlist();
    void updateLibrarylist();
    void updateModulelist();
};
#endif // MAINWINDOW_H
