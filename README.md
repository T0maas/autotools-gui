# About
This project is GUI program for autotools (configure.ac + Makefile.am stufs). You can create or open projects that use autotools
edit Makefile.am files and configure.ac files. You can create targets (programs, libraries) and edit parameters like: CFLAGS LDFLAGS and more. This program also allows adding *.pc file to your project, but this support is limited and you must edit pc file by hand (editing form included in program).

# Requirements
* Qt5
* libautotools (Get here: https://gitlab.com/T0maas/libautotools)

If you installed libautotools into /usr/local prefix and qmake can't find it, try this command before running qmake: 
`export PKG_CONFIG_PATH=/usr/local/lib/pkgconfig`