#ifndef QTREEWIDGETITEMCUSTOM_H
#define QTREEWIDGETITEMCUSTOM_H
#include <QTreeWidgetItem>
#include <makefile.h>
#include <configurator.h>

class QTreeWidgetItemCustom : public QTreeWidgetItem
{
    protected:

public:
    QTreeWidgetItemCustom(QTreeWidgetItem * treeview);
    autotools::Makefile_ref makefile;
    autotools::Target_ref target;
    autotools::PCFile_ref pcfile;
    ~QTreeWidgetItemCustom();
};

#endif // QTREEWIDGETITEMCUSTOM_H
