#include <sstream>
#include "stringops.h"


std::string  stringops::join(std::vector<std::string> vec) {
    std::ostringstream oss;
    for (auto const& s : vec) {
        if (!oss.str().empty()) {
            oss << ' '; // add space separator
        }
        oss << s;
    }
    return oss.str();
}
