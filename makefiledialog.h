#ifndef MAKEFILEDIALOG_H
#define MAKEFILEDIALOG_H

#include <QMainWindow>
#include "mainwindow.h"

namespace Ui {
class MakefileDialog;
}

class MakefileDialog : public QMainWindow
{
    Q_OBJECT

public:
    explicit MakefileDialog(QWidget *parent = nullptr);
    ~MakefileDialog();

private slots:


    void on_bt_storno_clicked();

    void on_bt_add_clicked();

private:
    Ui::MakefileDialog *ui;
    MainWindow * parent;
};

#endif // MAKEFILEDIALOG_H
