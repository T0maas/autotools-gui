#ifndef STRINGOPS_H
#define STRINGOPS_H
#include <vector>
#include <string>

namespace stringops {
    extern std::string join(std::vector<std::string> vec);
}
#endif // STRINGOPS_H
