#include "pcfiledialog.h"
#include "ui_pcfiledialog.h"

PCFileDialog::PCFileDialog(QWidget *parent) : QMainWindow(parent), ui(new Ui::PCFileDialog) {
    ui->setupUi(this);
    this->parent = (MainWindow*) parent;
}

PCFileDialog::~PCFileDialog() { delete ui; }

void PCFileDialog::on_bt_add_clicked()
{
    if (ui->tb_path->text().length() > 0)
        parent->add_pcfile(ui->tb_path->text());
    this->close();
}


void PCFileDialog::on_pushButton_clicked()
{
    this->close();
}

