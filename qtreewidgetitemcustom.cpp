#include "qtreewidgetitemcustom.h"
#include <QDebug>

QTreeWidgetItemCustom::QTreeWidgetItemCustom(QTreeWidgetItem *treeview) : QTreeWidgetItem(treeview) {}

QTreeWidgetItemCustom::~QTreeWidgetItemCustom() {
    qDebug() << "~QTreeWidgetItemCustom()\n";
}
