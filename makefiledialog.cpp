#include "makefiledialog.h"
#include "ui_makefiledialog.h"
#include <QDebug>

MakefileDialog::MakefileDialog(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MakefileDialog)
{
    ui->setupUi(this);
    this->parent = (MainWindow*) parent;
}

MakefileDialog::~MakefileDialog()
{
    delete ui;
}

void MakefileDialog::on_bt_storno_clicked()
{
    this->close();
}


void MakefileDialog::on_bt_add_clicked()
{
    if (ui->tb_path->text().length() > 0)
        parent->add_makefile(ui->tb_path->text() + "/Makefile" );
    else
        parent->add_makefile("Makefile" );
    this->close();
}


