#ifndef PCFILEDIALOG_H
#define PCFILEDIALOG_H

#include <QMainWindow>
#include "mainwindow.h"

namespace Ui {
class PCFileDialog;
}

class PCFileDialog : public QMainWindow {
    Q_OBJECT

public:
    explicit PCFileDialog(QWidget *parent = nullptr);
    ~PCFileDialog();

private slots:
    void on_bt_add_clicked();

    void on_pushButton_clicked();

private:
    Ui::PCFileDialog *ui;
    MainWindow * parent;
};

#endif // PCFILEDIALOG_H
