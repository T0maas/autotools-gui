#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <QFileDialog>
#include <QDebug>
#include <qtreewidgetitemcustom.h>
#include <QMessageBox>
#include <QStringListModel>
#include "pcfiledialog.h"
#include "makefiledialog.h"
#include "stringops.h"

QTreeWidgetItemCustom * treeAddItem(QTreeWidgetItem * tree, QString text) {
    QTreeWidgetItemCustom *item2=new QTreeWidgetItemCustom(tree);
    item2->setText(0,text);
    return item2;
}


MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    ui->splitter->setStretchFactor(1,2);
    ui->tabWidget->findChild<QTabBar*>()->hide();
    ui->tabWidget->setCurrentIndex(3);

}

void MainWindow::add_makefile(QString path)
{
    auto mkref = this->autocfg->addMakefile(path.toStdString());
    auto treeref =  treeAddItem(this->topLevelItem, path);
    treeref->makefile = mkref;
    treeref->setIcon(0, QIcon(":/icons/makefile.svg"));
}

void MainWindow::add_pcfile(QString path) {
    auto pcref = this->autocfg->addPCfile(path.toStdString());
    auto treeref = treeAddItem(this->topLevelItem, path);
    treeref->pcfile = pcref;
    treeref->setIcon(0, QIcon(":/icons/pcfile.svg"));
}

MainWindow::~MainWindow()
{
    delete ui;
}
void MainWindow::enableui() {
    ui->tabWidget->setCurrentIndex(0);
    ui->bt_addMakefile->setEnabled(true);
    ui->bt_addPCFile->setEnabled(true);
    ui->tb_email->setEnabled(true);
    ui->tb_projname->setEnabled(true);
    ui->tb_projvers->setEnabled(true);
}
void MainWindow::onNewProject() {

    this->autocfg = autotools::Configurator::CreateProject(".");
    ReloadProject();
    ui->startupnotify->setVisible(false);
    enableui();
}

void MainWindow::onOpenProject() {
    this->projdir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),projdir,QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks|QFileDialog::DontUseNativeDialog);

    try {
        this->autocfg =  autotools::Configurator::OpenProject(this->projdir.toStdString());
        this->ReloadProject();
        qDebug() << "Ok" << "\n";
        ui->startupnotify->setVisible(false);
        enableui();
    }
    catch(std::exception & exp) {

        QMessageBox msgbox;
        msgbox.setText(QString::fromStdString(exp.what()));
        msgbox.exec();
    }
}

void MainWindow::onSaveProject()
{
    this->projdir = QFileDialog::getExistingDirectory(this, tr("Open Directory"),projdir,QFileDialog::ShowDirsOnly| QFileDialog::DontResolveSymlinks|QFileDialog::DontUseNativeDialog);
    if (ui->cb_CXX->checkState() == Qt::Checked)
        autocfg->enableCXX(true);
    else
        autocfg->enableCXX(false);


    autocfg->setProjectPath(this->projdir.toStdString());
    autocfg->Write();
}

void MainWindow::ReloadProject() {
    ui->treeWidget->clear();
    this->topLevelItem = new QTreeWidgetItem(ui->treeWidget);

    //ui->treeWidget->addTopLevelItem(topLevelItem);
    topLevelItem->setText(0,"[project]");

    treeAddItem(topLevelItem, "configure.ac")->setIcon(0, QIcon(":/icons/configure.svg"));

    auto makefiles = autocfg->getMakefiles();
    auto pcfiles = autocfg->getPCFiles();

    for (auto mk : makefiles) {
        auto mkitem = treeAddItem(topLevelItem, QString::fromStdString(mk->getPath()));
        mkitem->setIcon(0, QIcon(":/icons/makefile.svg"));
        mkitem->makefile = mk;
        for (auto tar: mk->getTargets()) {
            auto tgnode = treeAddItem(mkitem, QString::fromStdString(tar->getName()));
            tgnode->target = tar;
            tgnode->setIcon(0, QIcon(":/icons/target.svg"));
        }
    }
    for (auto pc : pcfiles) {
        auto pcitem = treeAddItem(topLevelItem, QString::fromStdString(pc->getPath()));
        pcitem->setIcon(0, QIcon(":/icons/pcfile.svg"));
        pcitem->pcfile = pc;
    }
    ui->tb_projname->setText(QString::fromStdString(this->autocfg->getName()));
    ui->tb_projvers->setText(QString::fromStdString(this->autocfg->getVersion()));
    ui->tb_email->setText(QString::fromStdString(this->autocfg->getMail()));


    updateHeaderlist();
    updateLibrarylist();
    updateModulelist();
    if (autocfg->getCXX())
        ui->cb_CXX->setCheckState(Qt::Checked);
}

void MainWindow::configureac_selected() {

}

void MainWindow::makefile_selected( autotools::Makefile_ref mk ) {
    ui->tb_mmakepath->setText( QString::fromStdString( mk->getPath()));
    this->selected_makefile = mk;
    udpateMKsubdirlist();
}

void MainWindow::target_selected( autotools::Target_ref tg) {
    this->selected_target = tg;
    ui->tb_ttgname->setText(QString::fromStdString(tg->getName()));


    ui->tb_src->setText(QString::fromStdString(stringops::join(tg->getSources())));
    ui->tb_CPPFLAGS->setText(QString::fromStdString(stringops::join(tg->getCPPFLAGS())));
    ui->tb_CFLAGS->setText(QString::fromStdString(stringops::join(tg->getCFLAGS())));
    ui->tb_LDFLAGS->setText(QString::fromStdString(stringops::join(tg->getLDFLAGS())));
    ui->tb_LDADD->setText(QString::fromStdString(stringops::join(tg->getLDADD())));
    ui->tb_LIBADD->setText(QString::fromStdString(stringops::join(tg->getLIBADD())));

    auto tgtype = tg->getType();
    switch (tgtype) {
    case autotools::Target::Types::program:
        ui->cb_ttgtype->setCurrentIndex(0);
        ui->tb_LDADD->setEnabled(true);
        ui->tb_LIBADD->setEnabled(false);
        break;

    case autotools::Target::Types::library:
        ui->cb_ttgtype->setCurrentIndex(1);
        ui->tb_LDADD->setEnabled(false);
        ui->tb_LIBADD->setEnabled(true);
        break;
    default:
        break;
    }
}

void MainWindow::pcfile_selected(autotools::PCFile_ref pc){
    this->selected_pcfile = pc;
    ui->tb_pcfile->setPlainText( QString::fromStdString(pc->getContent()));
}

void MainWindow::on_treeWidget_itemClicked(QTreeWidgetItem *item, int column)
{
    if (this->topLevelItem == item) {
        qDebug() << "project click\n";
    }
    else {
        QTreeWidgetItemCustom * itemcustom = (QTreeWidgetItemCustom*) item;
        this->selected_node = itemcustom;
        qDebug() << "item clicked\n";
        if (itemcustom->makefile == nullptr && itemcustom->target == nullptr && itemcustom->pcfile == nullptr) {
           qDebug() << "configure.ac clicked\n" ;
           ui->tabWidget->setCurrentIndex(0);
           configureac_selected();
        }
        else if (itemcustom->makefile) {
           qDebug() << "Makefile clicked\n" ;
           ui->tabWidget->setCurrentIndex(1);
           makefile_selected(itemcustom->makefile);

        }
        else if (itemcustom->target) {
           qDebug() << "target clicked\n" ;
           ui->tabWidget->setCurrentIndex(2);
           target_selected(itemcustom->target);
        }
        else if (itemcustom->pcfile) {
           ui->tabWidget->setCurrentIndex(4);
           pcfile_selected(itemcustom->pcfile);
        }
    }
}


void MainWindow::on_bt_addMakefile_clicked()
{
   // QString makepath = ui->tb_cmakepath->text();
    auto  mkd = new MakefileDialog (this);
    mkd->setAttribute( Qt::WA_DeleteOnClose );
    mkd->show();
   /* if (makepath.endsWith("/Makefile") || makepath == "Makefile" ) {
        auto mkref =  this->autocfg.addMakefile(makepath.toStdString());
        treeAddItem(this->topLevelItem,ui->tb_mtgname->text())->makefile = mkref;
        //ReloadProject();
    }
    else {
        QMessageBox msgbox;
        msgbox.setText("Makefile path must be 'Makefile' or path must end with 'Makefile' (ex: src/Makefile)");
        msgbox.exec();
    }
*/
}


void MainWindow::on_bt_addtarget_clicked()
{
    if(selected_makefile) {
        autotools::Target::Types tgtype;

        if (ui->cb_mtgtype->currentText() == "prog")
           tgtype = autotools::Target::Types::program;
        else if (ui->cb_mtgtype->currentText() == "lib")
            tgtype = autotools::Target::Types::library;
        else
            tgtype = autotools::Target::Types::program;

        auto tgref = selected_makefile->addTarget(ui->tb_mtgname->text().toStdString(),tgtype, ui->tb_mtgprefix->currentText().toStdString());
       auto tritem =  treeAddItem(this->selected_node,ui->tb_mtgname->text());
       tritem->target = tgref;
       tritem->setIcon(0, QIcon(":/icons/target.svg"));
        //ReloadProject();
    }
}


void MainWindow::on_bt_delmk_clicked()
{
    this->autocfg->delMakefile(this->selected_makefile);
    delete this->selected_node;
    ui->tabWidget->setCurrentIndex(0);
}


void MainWindow::on_tb_projname_editingFinished()
{
    if (! this->autocfg)
       this->autocfg = autotools::Configurator::CreateProject(".");
    this->autocfg->setName(ui->tb_projname->text().toStdString());
}


void MainWindow::on_tb_mmakepath_editingFinished()
{
    this->selected_makefile->setPath(ui->tb_mmakepath->text().toStdString());
    this->selected_node->setText(0, ui->tb_mmakepath->text());
}


void MainWindow::on_tb_projvers_editingFinished()
{
    if (! this->autocfg)
       this->autocfg = autotools::Configurator::CreateProject(".");
    this->autocfg->setVersion(ui->tb_projvers->text().toStdString());
}


void MainWindow::on_tb_email_editingFinished()
{
    if (! this->autocfg)
       this->autocfg = autotools::Configurator::CreateProject(".");
    this->autocfg->setMail(ui->tb_email->text().toStdString());
}


void MainWindow::on_bt_maddsubdir_clicked()
{
    try {
        this->selected_makefile->addSubdir(ui->tb_maddsubdir->text().toStdString());
       udpateMKsubdirlist();
    }
    catch (std::exception & exp ) {
        QMessageBox msgbox;
        msgbox.setText(QString::fromStdString(exp.what()));
        msgbox.exec();
    }
}


void MainWindow::udpateMKsubdirlist() {
    auto model = qobject_cast<QStringListModel*>(ui->ls_msubdirs->model());
    if (!model) {
        model = new QStringListModel();
        ui->ls_msubdirs->setModel(model);
    }
    QStringList dataList;
    for (auto sb : this->selected_makefile->getSubdirs())  {
        dataList << QString::fromStdString(sb);
    }
    model->setStringList(dataList);
    ui->ls_msubdirs->show();
}

void MainWindow::updateLibrarylist() {
    auto model = qobject_cast<QStringListModel*>(ui->ls_libraries->model());
    if (!model) {
        model = new QStringListModel();
        ui->ls_libraries->setModel(model);
    }
    QStringList dataList;
    for (auto sb : this->autocfg->getCheckLibraries() )  {
        dataList << QString::fromStdString(sb.lib);
    }
    model->setStringList(dataList);
    ui->ls_libraries->show();

}

void MainWindow::updateHeaderlist() {
    auto model = qobject_cast<QStringListModel*>(ui->ls_headers->model());
    if (!model) {
        model = new QStringListModel();
        ui->ls_headers->setModel(model);
    }
    QStringList dataList;
    for (auto sb : this->autocfg->getCheckHeaders() )  {
        dataList << QString::fromStdString(sb.header);
    }
    model->setStringList(dataList);
    ui->ls_headers->show();
}



void MainWindow::updateModulelist() {
    auto model = qobject_cast<QStringListModel*>(ui->ls_modules->model());
    if (!model) {
        model = new QStringListModel();
        ui->ls_modules->setModel(model);
    }
    QStringList dataList;
    for (auto sb : this->autocfg->getCheckModules() )  {
        dataList << QString::fromStdString(sb.name);
    }
    model->setStringList(dataList);
    ui->ls_modules->show();
}

void MainWindow::on_ls_headers_clicked(const QModelIndex &index)
{
    int i = index.row();
    auto headers = this->autocfg->getCheckHeaders();
    auto header = headers[i];
    ui->tb_headername->setText(QString::fromStdString(header.header));
    ui->cb_headerfound->setCurrentText(QString::fromStdString(header.found));
    ui->cb_headernotfound->setCurrentText(QString::fromStdString(header.notfound));
}


void MainWindow::on_bt_addheader_clicked()
{
    std::string hname = ui->tb_headername->text().toStdString();
    auto hbegin = autocfg->getCheckHeaders().begin();
    auto hend = autocfg->getCheckHeaders().end();

    auto h_found = std::find_if(hbegin,hend,[hname](const autotools::Header_check & hdr){
        if (hdr.header == hname) {
            return true;
        }
        else return false;
    });
    if (h_found != hend) { //mod header
        int h_index =  h_found  - hbegin;
        auto & href =  autocfg->getHeader(h_index);
        href.found = ui->cb_headerfound->currentText().toStdString();
        href.notfound = ui->cb_headernotfound->currentText().toStdString();
    }
    else { //new header
        autotools::Header_check hdr;
        hdr.header = ui->tb_headername->text().toStdString();
        hdr.found = ui->cb_headerfound->currentText().toStdString();
        hdr.notfound = ui->cb_headernotfound->currentText().toStdString();
        this->autocfg->addCheckHeader(hdr.header,hdr.found,hdr.notfound);
        updateHeaderlist();
    }
}



void MainWindow::on_bt_delheader_clicked()
{
    auto qindex = ui->ls_headers->currentIndex();
    if (qindex.isValid()) {
        auto index  = qindex.row();
        this->autocfg->delHeader(index);
        updateHeaderlist();
    }
}





void MainWindow::on_bt_delsubdir_clicked()
{
    auto model = ui->ls_msubdirs->model();
    if (model) {
        auto index = ui->ls_msubdirs->currentIndex();
        if (index.isValid()) {
            auto myitem = index.data().toString();
            selected_makefile->delSubdir(myitem.toStdString());
            udpateMKsubdirlist();
        }
    }
}


void MainWindow::on_bt_addlib_clicked()
{
    std::string libname = ui->tb_library_name->text().toStdString();
    std::string libfn = ui->tb_library_function->text().toStdString();


    auto lbegin = autocfg->getCheckLibraries().begin();
    auto lend = autocfg->getCheckLibraries().end();

    auto l_found = std::find_if(lbegin,lend,[libname](const autotools::Library_check & _lib){
        if (_lib.lib == libname) {
            return true;
        }
        else return false;
    });
    if (l_found != lend) { //mod library
        int l_index =  l_found  - lbegin;
        auto & lref =  autocfg->getLibrary(l_index);
        lref.found = ui->cb_libraryfound->currentText().toStdString();
        lref.notfound = ui->cb_librarynotfound->currentText().toStdString();
        lref.func = ui->tb_library_function->text().toStdString();
    }
    else { //new library
        autotools::Library_check lbr;
        lbr.lib = ui->tb_library_name->text().toStdString();
        lbr.func = ui->tb_library_function->text().toStdString();
        lbr.found = ui->cb_libraryfound->currentText().toStdString();
        lbr.notfound = ui->cb_librarynotfound->currentText().toStdString();
        this->autocfg->addCheckLib(lbr.lib,lbr.func, lbr.found,lbr.notfound);
        updateLibrarylist();
    }

}



void MainWindow::on_ls_libraries_clicked(const QModelIndex &index)
{
    int i = index.row();
    auto libs = this->autocfg->getCheckLibraries();
    auto lib = libs[i];
    ui->tb_library_name->setText(QString::fromStdString(lib.lib));
    ui->cb_libraryfound->setCurrentText(QString::fromStdString(lib.found));
    ui->tb_library_function->setText(QString::fromStdString(lib.func));
    ui->cb_librarynotfound->setCurrentText(QString::fromStdString(lib.notfound));
}


void MainWindow::on_bt_dellib_clicked()
{
    auto qindex = ui->ls_libraries->currentIndex();

    if (qindex.isValid()) {
        auto index  = qindex.row();
        this->autocfg->delLib(index);
        updateLibrarylist();
    }
}


void MainWindow::on_tb_headername_editingFinished()
{

}


void MainWindow::on_bt_addmod_clicked()
{
    std::string modname = ui->tb_modulename->text().toStdString();
    std::string modmac = ui->tb_modulevar->text().toStdString();


    auto mbegin = autocfg->getCheckModules().begin();
    auto mend = autocfg->getCheckModules().end();

    auto m_found = std::find_if(mbegin,mend,[modname](const autotools::Module_check & _mod){
        if (_mod.name == modname) {
            return true;
        }
        else return false;
    });
    if (m_found != mend) { //mod module
        int l_index =  m_found  - mbegin;
        auto & mref =  autocfg->getModule(l_index);
        mref.name = ui->tb_modulename->text().toStdString();
        mref.prefix = ui->tb_modulevar->text().toStdString();
    }
    else { //new module
        autotools::Module_check mod;
        mod.name = ui->tb_modulename->text().toStdString();
        mod.prefix = ui->tb_modulevar->text().toStdString();
        this->autocfg->addModule(mod.prefix,mod.name);
        updateModulelist();
    }
}


void MainWindow::on_ls_modules_clicked(const QModelIndex &index)
{
    int i = index.row();
    auto mods = this->autocfg->getCheckModules();
    auto mod = mods[i];
    ui->tb_modulename->setText(QString::fromStdString(mod.name));
    ui->tb_modulevar->setText(QString::fromStdString(mod.prefix));
}


void MainWindow::on_bt_delmod_clicked()
{
    auto qindex = ui->ls_modules->currentIndex();

    if (qindex.isValid()) {
        auto index  = qindex.row();
        this->autocfg->delModule(index);
        updateModulelist();
    }
}


void MainWindow::on_tb_src_editingFinished()
{
    auto tx_sources = ui->tb_src->text();
    auto sources = tx_sources.split(" ");
    std::vector<std::string> sources_list;
    for (QString s : sources) {
        if (s.length()) {
            sources_list.push_back(s.toStdString());
        }
    }
    this->selected_target->clearSources();
    this->selected_target->addSources(sources_list);
}


void MainWindow::on_bt_addPCFile_clicked()
{
    auto  mkd = new PCFileDialog (this);
    mkd->setAttribute( Qt::WA_DeleteOnClose );
    mkd->show();
}


void MainWindow::on_tb_pcfile_textChanged()
{
    selected_pcfile->setContent(ui->tb_pcfile->toPlainText().toStdString());
}




void MainWindow::on_tb_CPPFLAGS_editingFinished()
{
    auto flags = ui->tb_CPPFLAGS->text();
    auto spl = flags.split(" ");
    std::vector<std::string> lst;

    for (QString s : spl) {
        if (s.length()) {
            lst.push_back(s.toStdString());
        }
    }
    this->selected_target->clearCPPFLAGS();
    this->selected_target->addCPPFLAGS(lst);
}


void MainWindow::on_tb_CFLAGS_editingFinished()
{
    auto flags = ui->tb_CFLAGS->text();
    auto spl = flags.split(" ");
    std::vector<std::string> lst;

    for (QString s : spl) {
        if (s.length()) {
            lst.push_back(s.toStdString());
        }
    }
    this->selected_target->clearCFLAGS();
    this->selected_target->addCFLAGS(lst);
}


void MainWindow::on_tb_LDFLAGS_editingFinished()
{
    auto flags = ui->tb_LDFLAGS->text();
    auto spl = flags.split(" ");
    std::vector<std::string> lst;

    for (QString s : spl) {
        if (s.length()) {
            lst.push_back(s.toStdString());
        }
    }
    this->selected_target->clearLDFLAGS();
    this->selected_target->addLDFLAGS(lst);
}


void MainWindow::on_tb_LDADD_editingFinished()
{
    auto flags = ui->tb_LDADD->text();
    auto spl = flags.split(" ");
    std::vector<std::string> lst;

    for (QString s : spl) {
        if (s.length()) {
            lst.push_back(s.toStdString());
        }
    }
    this->selected_target->clearLDADD();
    this->selected_target->addLDADD(lst);
}


void MainWindow::on_tb_LIBADD_editingFinished()
{
    auto flags = ui->tb_LIBADD->text();
    auto spl = flags.split(" ");
    std::vector<std::string> lst;

    for (QString s : spl) {
        if (s.length()) {
            lst.push_back(s.toStdString());
        }
    }
    this->selected_target->clearLIBADD();
    this->selected_target->addLIBADD(lst);
}

